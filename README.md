# Assignment3

## .NET Course - MovieDB 🎥

This is a project made in collaboration between Michael Kosther(Kostmic) and Kristian Garberg(Sirtakin). 

This is a web API created with ASP.NET Core. It is used to both setup and manipulate a MovieDB database.

The given database has 3 distinct tables
* Movie (Contains general information about a movie)
* Franchise (Contains information about a franchise and which movies it contains)
* Character (Contains information about a character and which movies the character appears in)

And endpoints to manipulate data in any of the given tables. The swagger documentation will give further insight into which endpoints can be utilized and how they can be used. 

### Project setup
When setting up this project on your local machine there are two important steps 
1. Change the db connection string
2. Setup the local database

#### Change the db connection string
This can be done by the file named appsettings.json inside the Assignment3 folder and setting the Data Source to whatever the name of your Server is. In SSMS this can be done by right clicking the server and clicking 'Properties'.
The Server name will be under the Name category. This name can then be pasted into the appsettings.json under the aforementioned 'Data source' part of the given string. In the example under **\<NewDataSource>** can be replaced with the given data source

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=<NewDataSource>; Initial Catalog= MovieDB; Integrated Security=True;"
  }
}
```

#### Setup the local database
To setup the local database you need to open Visual Studio and click Tools > Nuget Package Manager > Package Manager Console.
Write this command in the console:
```
update-database 
```

When this is done you should be able to start the web server and both test it and view the documentation in the accompanying swagger documentation.
