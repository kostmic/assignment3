using Assignment3.Models;
using Assignment3.Models.DTO.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>().ForMember(cdto => cdto.Movies, opt => opt.MapFrom(c => c.Movies.Select(m => m.Id).ToArray()));

            CreateMap<CharacterCreateDTO, Character>();//.ForMember(fdto => fdto.Movies, opt => opt.MapFrom(f => f.Movies.Select(m => m.Id))).ReverseMap();

            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
