using Assignment3.Models.DTO.Movie;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Profiles
{
  public class MovieProfile : Profile
  {
    public MovieProfile()
    {
      CreateMap<Movie, MovieReadDTO>().ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.Characters.Select(c => c.Id).ToArray()));

            CreateMap<MovieCreateDTO, Movie>().ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.MovieTitle)).ReverseMap();

      CreateMap<MovieEditDTO, Movie>();//.ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.CharacterI);
    }
  }
}
