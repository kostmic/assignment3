﻿// <auto-generated />
using Assignment3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Assignment3.Migrations
{
    [DbContext(typeof(MovieDBContext))]
    [Migration("20210901143523_MoviesDBb")]
    partial class MoviesDBb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Assignment3.Models.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alias")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Gender")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Picture")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Character");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "Iron Man",
                            FullName = "Robert Downey Jr.",
                            Gender = "Male",
                            Picture = "Ironman.png"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Thor",
                            FullName = "Chris Hemsworth",
                            Gender = "Male",
                            Picture = "Hammerman.png"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "Black Widow",
                            FullName = "Scarlett Johansson",
                            Gender = "Female",
                            Picture = "Spooder.png"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "Batman",
                            FullName = "Christian Bale",
                            Gender = "Male",
                            Picture = "Batsy.png"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("MoviesId")
                        .HasColumnType("int");

                    b.Property<int>("CharactersId")
                        .HasColumnType("int");

                    b.HasKey("MoviesId", "CharactersId");

                    b.HasIndex("CharactersId");

                    b.ToTable("CharacterMovie");

                    b.HasData(
                        new
                        {
                            MoviesId = 1,
                            CharactersId = 2
                        },
                        new
                        {
                            MoviesId = 1,
                            CharactersId = 3
                        },
                        new
                        {
                            MoviesId = 2,
                            CharactersId = 1
                        },
                        new
                        {
                            MoviesId = 3,
                            CharactersId = 4
                        });
                });

            modelBuilder.Entity("Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(2000)
                        .HasColumnType("nvarchar(2000)");

                    b.Property<string>("Name")
                        .HasMaxLength(40)
                        .HasColumnType("nvarchar(40)");

                    b.HasKey("Id");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "The Marvel universe",
                            Name = "Marvel"
                        },
                        new
                        {
                            Id = 2,
                            Description = "The DC Univers",
                            Name = "DC"
                        });
                });

            modelBuilder.Entity("Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Director")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<int>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("MovieTitle")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Picture")
                        .HasMaxLength(40)
                        .HasColumnType("nvarchar(40)");

                    b.Property<int>("ReleaseYear")
                        .HasMaxLength(4)
                        .HasColumnType("int");

                    b.Property<string>("Trailer")
                        .HasMaxLength(40)
                        .HasColumnType("nvarchar(40)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Russo Brothers",
                            FranchiseId = 0,
                            Genre = "Action",
                            MovieTitle = "Avengers: End Game",
                            Picture = "Endgame.png",
                            ReleaseYear = 2018,
                            Trailer = "www.youtube.com/endgametrailer"
                        },
                        new
                        {
                            Id = 2,
                            Director = "John Favreau",
                            FranchiseId = 0,
                            Genre = "Action",
                            MovieTitle = "Iron man",
                            Picture = "Ironboi.png",
                            ReleaseYear = 2008,
                            Trailer = "www.youtube.com/IronManTrailer"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Christopher Nolan",
                            FranchiseId = 0,
                            Genre = "Action, Crime, Drama",
                            MovieTitle = "The Dark Knight",
                            Picture = "Batboi.png",
                            ReleaseYear = 2008,
                            Trailer = "www.youtube.com/DarkKnight"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("Assignment3.Models.Character", null)
                        .WithMany()
                        .HasForeignKey("CharactersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Movie", null)
                        .WithMany()
                        .HasForeignKey("MoviesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Movie", b =>
                {
                    b.HasOne("Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
