﻿using Assignment3.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

public class Movie
{
    public int Id { get; set; }
    [MaxLength(50)]
    public string MovieTitle { get; set; }
    [MaxLength(20)]
    public string Genre { get; set; }
    [MaxLength(4)]
    public int ReleaseYear { get; set; }
    [MaxLength(20)]
    public string Director { get; set; }
    [MaxLength(40)]
    public string Picture { get; set; }
    [MaxLength(40)]
    public string Trailer { get; set; }
    public int FranchiseId { get; set; }
    public Franchise Franchise { get; set; }
    public ICollection<Character> Characters { get; set; }
}
