﻿using Assignment3.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
public class Franchise
{
    public int Id { get; set; }
    [MaxLength(40)]
    public string Name { get; set; }
    [MaxLength(2000)]
    public string Description { get; set; }
    public ICollection<Movie> Movies { get; set;}
}
