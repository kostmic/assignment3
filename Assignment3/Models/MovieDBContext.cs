﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment3.Models;
using System.Diagnostics.CodeAnalysis;

namespace Assignment3.Models
{
    public class MovieDBContext : DbContext
    {
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        public MovieDBContext( DbContextOptions options) : base(options)
        {
        }
        /// <summary>
        /// Creating some dummy data for our database
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, MovieTitle = "Avengers: End Game", Genre = "Action", ReleaseYear = 2018, Director = "Russo Brothers", Picture = "Endgame.png", Trailer = "www.youtube.com/endgametrailer", FranchiseId = 1});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, MovieTitle = "Iron man", Genre = "Action", ReleaseYear = 2008, Director = "John Favreau", Picture = "Ironboi.png", Trailer = "www.youtube.com/IronManTrailer", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, MovieTitle = "The Dark Knight", Genre = "Action, Crime, Drama", ReleaseYear = 2008, Director = "Christopher Nolan", Picture = "Batboi.png", Trailer = "www.youtube.com/DarkKnight", FranchiseId = 2 });

            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Robert Downey Jr.", Alias = "Iron Man", Gender = "Male", Picture = "Ironman.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Chris Hemsworth", Alias = "Thor", Gender = "Male", Picture = "Hammerman.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Scarlett Johansson", Alias = "Black Widow", Gender = "Female", Picture = "Spooder.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Christian Bale", Alias = "Batman", Gender = "Male", Picture = "Batsy.png" });

            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Marvel", Description = "The Marvel universe" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "DC", Description = "The DC Univers" });

            modelBuilder.Entity<Movie>()
                .HasMany(c => c.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    je =>
                    {
                        je.HasKey("MoviesId", "CharactersId");
                        je.HasData(
                            new { MoviesId = 1, CharactersId = 1 },
                            new { MoviesId = 1, CharactersId = 2 },
                            new { MoviesId = 1, CharactersId = 3 },
                            new { MoviesId = 2, CharactersId = 1 },
                            new { MoviesId = 3, CharactersId = 4 }
                        );
                    });

            //modelBuilder.Entity<Franchise>()
            //    .HasMany(m => m.Movies)
            //    .WithOne(f => f.Franchise);
        }

    }
}
