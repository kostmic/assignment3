using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using AutoMapper;
using Assignment3.Models.DTO.Franchise;
using Assignment3.Models.DTO.Character;
using Assignment3.Models.DTO.Movie;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDBContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the characters in the database.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchise.Include(m => m.Movies).ToListAsync());
        }

        /// <summary>
        /// Gets a specific character by their Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.Include(m => m.Movies).SingleOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all movies in a franchise by the franchise Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            Franchise franchise = await _context.Franchise.Include(m => m.Movies).Where(f => f.Id == id).FirstOrDefaultAsync();
            if (franchise == null)
            {
                return NotFound();
            }
            foreach (Movie movie in franchise.Movies)
            {
                movie.Franchise = null;
            }

            return _mapper.Map<List<MovieReadDTO>>(franchise.Movies.ToList());
        }

        /// <summary>
        /// Gets all characters in all movies in a franchise by the franchise Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharctersInMoviesInFranchise(int id)
        {
            Movie movie = await _context.Movie.Include(m => m.Characters).Where(f => f.FranchiseId == id).FirstOrDefaultAsync();
            if (movie == null)
            {
                return NotFound();
            }

            foreach (Character character in movie.Characters)
            {
                character.Movies = null;
            }

            return _mapper.Map<List<CharacterReadDTO>>(movie.Characters.ToList());
        }







        /// <summary>
        /// Updates a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new franchise to the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Updates a specific franchise's movies by the franchise Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMovieFranchise(int id, List<int> Movies)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise franchiseToUpdateMovies = await _context.Franchise
                .Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstAsync();

            List<Movie> newMovies = new();
            foreach (int movId in Movies)
            {
                Movie currMovie = await _context.Movie.FindAsync(movId);
                if (currMovie == null)
                    return BadRequest("Movie does not exist!");
                newMovies.Add(currMovie);
            }
            franchiseToUpdateMovies.Movies = newMovies;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.Id == id);
        }
    }
}
